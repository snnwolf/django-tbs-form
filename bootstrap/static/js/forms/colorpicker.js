/**
 * User: snn
 * Date: 08.11.12
 * Time: 15:25
 * Каждая строка с элементом colorpicker должна иметь класс .js-colorpicker
 * input: .colorpicker-input
 * preview elemnt: .colorpicker-preview
 *
 */

$(document).ready(function(){
    $('.js-colorpicker').each(function(){
        var $root = $(this),
            $element = $root.find('.colorpicker-input'),
            $preview = $root.find('.colorpicker-preview'),
            $input = $element.find('input');

        $element.data('color', $input.val() || '#ffffff')
        $preview.css({'background-color':$input.val()});
        $element.colorpicker()
            .on('changeColor', function(ev) {
                    $input.val(ev.color.toHex());
                    $preview.css({'background-color':$input.val()});
                })
        $input.on('change', function(){
            $element.data('color', $input.val())
            $preview.css({'background-color':$input.val()});
            $element.colorpicker('update')
        })
    })
});
