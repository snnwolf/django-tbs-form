/* =================================================
 * jquery.bootstrap-form-wiard.js v0.1
 * =================================================
 * Обработка форм. Отправляет ajax-POST запрос на form.action.
 * Ошибки полей, ошибки формы, сообщение размещает в диве .js-alert (по-умолчанию) под формой
 * Плагин писался для
 * css: http://twitter.github.com/bootstrap
 * django: git://github.com/earle/django-bootstrap
 *
 * Требования к форме
 * - аттрибут action
 *
 * Формат ответа (json):
 *  bool success (обязательно)
 *  array error || string message || string info (иначе будет вызван success из параметров)
 */
!(function($){
    "use strict"; // jshint ;_;
    var BootstrapFormWizard = function(element, options){
        this.init('bootstrapFormWizard', element, options);
    };
    /**
     * Для отладки методом alert(obj) в IE
     * @param object
     */
    var alertObject = function(object) {
        var output = '';
        for (var i in object) {
            if (typeof(object[i]) == 'function') continue
            output += i + ': ' + object[i]+'; ';
        }
        alert(output);
    }

    BootstrapFormWizard.prototype = {
        constructor: BootstrapFormWizard,
        init: function(type, element, options) {
            var self = this;
            self.type = type;
            self.$element = $(element);
            self.options = self.getOptions(options);
            self.enabled = true;
            self.errors = {};
            self.locked = false;
            // fix для б.. IE
            if(!self.$element.attr('action')) {
                self.$element.attr('action', window.location.href);
            }

            // контейнер сообщений
            var $alertContainer = self.$element.find('.'+self.options.containerClass);
            if (!$alertContainer.length) {
                self.$element.append($('<'+self.options.containerTag+'/>', {
                    'class':self.options.containerClass
                }))
            }
            // обработчики
            self.$element.submit(function(){
                if (self.locked) return false;
                self.locked = true;
                var $form = self.$element;
                // очищаем ошибки
                self.clearErrors();
                self.closeAlert();
                $.post($form.attr('action'), $form.serialize(), function(json){
                    if (json.errors) {
                        self.options.hasError && self.options.hasError($form, json);
                        $.each(json.errors, function(i,v){
                            self.addError(i, v);
                        });
                        self.showErrors();
                    } else if(json.success && json.message) {
                        self.alert({
                            'class':'alert-success',
                            'content':json.message
                        });
                    } else if (json.success && json.info) {
                        self.alert({
                            'class':'alert-info',
                            'content':json.info
                        });
                    }
                    if (!json.errors && json.success && self.options.success)
                        self.options.success($form, json);
                }, 'json')
                    .error(function(response) {
                        self.options.error && self.options.error($form, response);
                    })
                    .complete(function(xhr, status){
                        self.locked = false;
                        self.options.complete && self.options.complete($form, xhr, status);
                    });
                return false;
            });
        },
        getOptions: function (options) {
            options = $.extend({}, $.fn[this.type].defaults, options, this.$element.data());
            return options;
        },
        addError:function(element, errors) {
            this.errors[element] = errors;
        },
        showErrorInput: function(element) {
            var $form = this.$element
                , field_name = this.options.fieldPrefix ? this.options.fieldPrefix + '-' + element : element
                , $field_row = $form.find('#div_id_'+field_name);
            if ($field_row.length == 0) {
                $field_row = $form.find('[name='+field_name+']').parents('.control-group');
            }
            $field_row.addClass('error');
        },
        showErrors: function(errors){
            var self = this, error_list = [];

            self.clearInputErrors();
            self.closeAlert();
            errors = $.extend({}, self.errors, errors || {});

            $.each(errors,function(i,v){
                self.showErrorInput(i);
                if (i === '__all__') return;
                $.merge(error_list, v);
            });
            var _e = [];
            $.each(error_list, function(i,k){
                if(k && _e.indexOf(k) < 0) {
                    _e.push(k);
                }
            })
            error_list = _e;
            // показываем ошибки с полей
            if (error_list.length == 0 && errors['__all__'] && errors['__all__'].length > 0) {
                error_list = errors['__all__'];
            }
            this.alert({
                'class':'alert-error',
                'content':error_list.join('<br>')
            });
        },

        /**
         * Убрать подсветку с полей с ошибками и очистить сообщения под формой
         */
        clearErrors:function() {
            this.errors = [];
            this.clearInputErrors();
        },
        clearInputErrors:function(){
            var $form = this.$element;
            $form.find('.control-group.error').removeClass('error');
        },
        closeAlert:function() {
            var $form = this.$element;
            $form.find('.js-alert .alert').alert('close');
        },
        alert:function(message, title, params) {
            var self = this, directElement = this.$element.find('.'+this.options.containerClass);
            if (directElement.length === 0) {
                directElement = $('<div/>', {
                    'class':this.options.containerClass
                });
                this.$element.append(directElement);
            }
            if (typeof message == 'object') {
                params = $.extend({}, message);
                message = params['content'];
                title = params['title'];
            }
            params = $.extend({}, this.options, params);

            var div = $('<'+params.msgTag+'/>', {
                'class':'alert'
            });
            div.addClass(params['class']);
            if (params.x) {
                div.append('<a href="#" class="close" data-dismiss="alert">'+params.x+'</a>');
            }
            if (title) {
                div.append('<h4 class="alert-heading">'+title+'</h4>');
            }
            div.append(message);
            directElement.append(div);
            div.bind('closed',function(){self.clearErrors()})
        },
        disableSubmit:function(form, enable) {
            if (enable === undefined || enable) {
                $(form).find('[type=submit]').attr({disabled:'disabled'});
            }
            else {
                $(form).find('[type=submit]').removeAttr('disabled');
            }
        }

    };


    /**
     * @memberOf jQuery
     * @param method
     * @return {*}
     */
    $.fn.bootstrapFormWizard = function(method, option) {
        return this.each(function(){
            var $this = $(this),
                data = $this.data('bootstrapwizard'),
                options = typeof method == 'object' && method;
            if (!data) $this.data('bootstrapwizard', (data = new BootstrapFormWizard(this, options)));
            if (typeof method == 'string') data[method](option);
        });
    };

    $.fn.bootstrapFormWizard.constructor = BootstrapFormWizard;

    $.fn.bootstrapFormWizard.defaults = {
        containerClass:'js-alert' // контейнер для сообщений
        , containerTag:'div'      // тег контейнера для сообщений (будет создан в случае отсутствия в коде)
        , msgTag:'span'           // тег для сообщения может быть div (на всю ширину) или span (по ширине текста)
        , hasError:null           // функция на случай ошибок в форме
        , success:null            // функция на случай удачного запроса. Будет вызван с параметрами ($form, jsonResponse)
        , complete:null           // функция вызывается после завершения аякс запроса. аргументы: (form, xhr, statusText)
        , error:null              // функция на случай неудачного запроса. Будет вызван с параметрами ($form, jsonResponse)
        , 'x':'×'                 // закрывашка для сообщения
        , 'class':'alert-info'    // класс сообщения
        , 'content':'!!Alert!!'   // сообщение по-умолчанию
        , 'title':''              // заголовок сообщения по-умолчанию
        , 'fieldPrefix':''        // префикс к именам полей. Для FormWizard
    };
})(window.jQuery);
