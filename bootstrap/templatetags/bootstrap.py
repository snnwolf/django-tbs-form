from django.template import Context
from django.template.loader import get_template, select_template
from django import template
from django.utils.encoding import force_unicode
from django.utils.safestring import mark_safe

register = template.Library()

@register.filter
def bootstrap(element):
    element_type = element.__class__.__name__.lower()
    #print type(element.field.widget).__name__.lower()

    if element_type == 'boundfield':
        #template = get_template("bootstrap/field.html")
        bf = element.field

        if len(element.errors):
            # If the field contains errors, render the errors to a <ul>
            # using the error_list helper function.
            # bf_errors = error_list([escape(error) for error in bf.errors])
            bf_errors = ', '.join([e for e in element.errors])
        else:
            bf_errors = ''

        css_class = type(bf).__name__ + " " +  type(bf.widget).__name__
        # Add an extra class, Required, if applicable
        if bf.required:
            css_class += " required"

        if element.help_text:
            # The field has a help_text, construct <span> tag
            help_text = '<span class="help-%s">%s</span>' % ('inline', force_unicode(element.help_text))
        else:
            help_text = u''


        field_hash = {
            'class' : mark_safe(css_class),
            'label' : mark_safe(bf.label or ''),
            'help_text' :mark_safe(help_text),
            'field' : bf,
            'bf' : mark_safe(unicode(element)),
            'bf_raw' : element,
            'bf_data': element.data or element.form.initial.get(element.name),
            'errors' : mark_safe(bf_errors),
            'field_type' : mark_safe(element.__class__.__name__),
            'label_id': element._auto_id(),
        }

        template = select_template([
            'bootstrap/field_%s.html' % type(element.field.widget).__name__.lower(),
            'bootstrap/field_default.html',
        ])
        context = Context(field_hash)
    else:
        has_management = getattr(element, 'management_form', None)
        if has_management:
            template = get_template("bootstrap/formset.html")
            context = Context({'formset': element})
        else:
            template = get_template("bootstrap/form.html")
            context = Context({'form': element})
        
    return template.render(context)

@register.filter
def is_checkbox(field):
    return field.field.widget.__class__.__name__.lower() == "checkboxinput"


@register.filter
def is_radio(field):
    return field.field.widget.__class__.__name__.lower() == "radioselect"
