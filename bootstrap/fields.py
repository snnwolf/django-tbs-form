# -*- coding: utf-8 -*-
import datetime
from django.core.validators import RegexValidator
import re
import time
from django.core import validators
from django.core.exceptions import ValidationError
from django import forms
from django.forms import MultiValueField, DateField
from bootstrap import widgets as bootstrap_widget

__author__ = 'snn'

class UploadFileField(forms.MultiValueField):
    widget = bootstrap_widget.UploadFile
    error_messages = {
        'reqired':u'Обязательно для заполнения',
        'invalid_pk':'Invalid pk file',
        'invalid_url':'Invalid file url',
        'invalid_name':'Invalid file name',
    }

    def __init__(self, upload_url=None, max_file_size=None, file_extensions=None,
                 *args, **kwargs):
        self.upload_url = upload_url
        self.max_file_size = max_file_size
        self.file_extensions = file_extensions

        errors = self.default_error_messages.copy()
        if 'error_messages' in kwargs:
            errors.update(kwargs['error_messages'])
        fields = (
            forms.IntegerField(), # media pk
            forms.CharField(),    # url media
            forms.CharField(),    # media file name
        )
        super(UploadFileField, self).__init__(fields, *args, **kwargs)

    def to_python(self, value):
        if value:
            pk, url, name = value.split('$$')
            return int(pk), url, name
        return None

#    def validate(self, value):
#        super(UploadFileField, self).validate(value)


    def compress(self, data_list):
        """
        @type data_list: list
        @param data_list: pk, url, source_name
        """
        if data_list:
            if data_list[0] in validators.EMPTY_VALUES:
                raise ValidationError(self.error_messages['invalid_pk'])
            if data_list[1] in validators.EMPTY_VALUES:
                raise ValidationError(self.error_messages['invalid_url'])
            if data_list[2] in validators.EMPTY_VALUES:
                raise ValidationError(self.error_messages['invalid_name'])
            return '$$'.join(['%s' % d for d in data_list])
        return None

#    def get_url(self):
#        return self.fields[1]

class DateRangeField(MultiValueField):
    widget = bootstrap_widget.DateRangeWidget
    hidden_widget = bootstrap_widget.DateHiddenRangeWidget
    default_error_messages = {
        'invalid_date': u'Введите правильную дату',
        'start_less_finish':u'Дата начала должна быть не позднее даты окончания',
        'required':u'Заполните обязательные поля',
    }

    def __init__(self, input_date_formats=None, *args, **kwargs):
        errors = self.default_error_messages.copy()
        if 'error_messages' in kwargs:
            errors.update(kwargs['error_messages'])
        localize = kwargs.get('localize', False)
        fields = (
            DateField(input_formats=input_date_formats,
                      error_messages={'invalid': errors['invalid_date']},
                      localize=localize),
            DateField(input_formats=input_date_formats,
                      error_messages={'invalid': errors['invalid_date']},
                      localize=localize),
        )
        super(DateRangeField, self).__init__(fields, *args, **kwargs)

    def to_python(self, value):
        if value:
            values = map(float, value.split('-'))
            return map(lambda x: datetime.date.fromtimestamp(x), values)
        return None

    def to_datetime(self, value):
        if value:
            values = map(float, value.split('-'))
            return map(lambda x: datetime.datetime.fromtimestamp(x), values)
        return None

    def compress(self, data_list):
        if data_list:
            # Raise a validation error if date is empty
            # (possible if DateRangeField has required=False).
            if data_list[0] in validators.EMPTY_VALUES or  data_list[1] in validators.EMPTY_VALUES:
                raise ValidationError(self.error_messages['invalid_date'])
            try:
                date1 = time.mktime(data_list[0].timetuple()) # начать в полночь
                date2 = time.mktime(data_list[1].timetuple()) + 60*60*24 - 1 # закончить 23:59:59 указанного дня
            except ValueError:
                raise ValidationError(self.error_messages['invalid_date'])

            if date1 > date2:
                raise ValidationError(self.error_messages['start_less_finish'])

            return '-'.join(['%s'%date1, '%s'%date2])

        return None
