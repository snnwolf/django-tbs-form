# -*- coding: utf-8 -*-
from datetime import date
from django.forms import MultipleHiddenInput, DateInput
from django.forms.widgets import Input, RadioInput, RadioFieldRenderer, RadioSelect, TextInput, MultiWidget, Select, HiddenInput, CheckboxInput
from django.utils.html import conditional_escape
from django.utils.encoding import force_unicode
from django.utils.safestring import mark_safe


class OptionsRadioInput(RadioInput):
    def __unicode__(self):
        if 'id' in self.attrs:
            label_for = ' for="%s_%s"' % (self.attrs['id'], self.index)
        else:
            label_for = ''
        # snn:
        label_class = self.attrs.get('label_class', 'radio')
        label_for += ' class="%s"' % (label_class,)

        choice_label = conditional_escape(force_unicode(self.choice_label))
        return mark_safe(u'<label%s>%s %s</label>' %
                         (label_for, self.tag(), choice_label))


class OptionsRadioRenderer(RadioFieldRenderer):
    def __iter__(self):
        for i, choice in enumerate(self.choices):
            yield OptionsRadioInput(self.name, self.value, self.attrs.copy(), choice, i)

    def __getitem__(self, idx):
        choice = self.choices[idx] # Let the IndexError propogate
        return OptionsRadioInput(self.name, self.value, self.attrs.copy(), choice, idx)

    def render(self):
        """Outputs a simple list labels for this set of radio fields."""
        return mark_safe(u'\n'.join([u'%s' % force_unicode(w) for w in self]))
#        return mark_safe(u'<ul class="inputs-list">\n%s\n</ul>' %
#                         u'\n'.join([u'<li>%s</li>' %
#                         force_unicode(w) for w in self]))

class OptionsRadio(RadioSelect):
    renderer = OptionsRadioRenderer


class AppendedText(TextInput):
    """
    Добавляет иконку к полю справа
    """
    def render(self, name, value, attrs=None):
        append_text = self.attrs.get('text', '')
        tooltip_text = self.attrs.get('tooltip_text', '')
        tooltip_class = self.attrs.get('tooltip_class', '')
        tooltip_placement = self.attrs.get('tooltip_placement', 'right')
        if tooltip_text and tooltip_class:
            tooltip_text = u'<i rel="tooltip" data-placement="%s" data-animation="true" data-original-title="%s" class="%s"></i>' % (tooltip_placement, tooltip_text, tooltip_class)
        return mark_safe(u'%s<span class="add-on">%s</span>' % (super(AppendedText, self).render(name, value, attrs),
                                                     tooltip_text or append_text))


class PrependedText(TextInput):
    """
    Добавляет иконку к полю слева
    """
    def render(self, name, value, attrs=None):
        prepend_text = self.attrs.get('text', '')
        return mark_safe(u'<span class="add-on">%s</span>%s' % (prepend_text, super(PrependedText, self).render(name, value, attrs)))


class AppendPrependText(TextInput):
    """
    Добавляет иконку к полю справа и слева
    """
    def render(self, name, value, attrs=None):
        append_text, prepend_text = self.attrs.get('append_text', ''), self.attrs.get('prepend_text', '')
        return mark_safe(u'<span class="add-on">%s</span>%s<span class="add-on">%s</span>' % (prepend_text, super(AppendPrependText, self).render(name, value, attrs), append_text))


class EmailInput(Input):
    input_type = 'email'
    def render(self, name, value, attrs=None):
        append_text = self.attrs.get('text', '@')
        tooltip_text = self.attrs.get('tooltip_text', '')
        tooltip_class = self.attrs.get('tooltip_class', '')
        tooltip_placement = self.attrs.get('tooltip_placement', 'right')
        if tooltip_text and tooltip_class:
            tooltip_text = u'<i rel="tooltip" data-placement="%s" data-animation="true" data-original-title="%s" class="%s"></i>' % (tooltip_placement, tooltip_text, tooltip_class)
        return mark_safe(u'%s<span class="add-on">%s</span>' % (super(EmailInput, self).render(name, value, attrs),
                                                     tooltip_text or append_text))

class PasswordInput(Input):
    input_type = 'password'
    def render(self, name, value, attrs=None):
        append_text = self.attrs.get('text', '@')
        tooltip_text = self.attrs.get('tooltip_text', '')
        tooltip_class = self.attrs.get('tooltip_class', '')
        tooltip_placement = self.attrs.get('tooltip_placement', 'right')
        if tooltip_text and tooltip_class:
            tooltip_text = u'<i rel="tooltip" data-placement="%s" data-animation="true" data-original-title="%s" class="%s"></i>' % (tooltip_placement, tooltip_text, tooltip_class)
        return mark_safe(u'%s<span class="add-on">%s</span>' % (super(PasswordInput, self).render(name, value, attrs),
                                                     tooltip_text or append_text))


class BetweenWidget(MultiWidget):
    """
    A Widget that splits text input into two <input type="text"> boxes.
    """

    def __init__(self, attrs=None, choices=None):
        widgets = ((
                       Select(attrs=attrs,choices=choices),
                       Select(attrs=attrs,choices=choices)
                       ))
        super(BetweenWidget, self).__init__(widgets, attrs)

    def decompress(self, value):
        if value:
            return value.split('-')
        return [None, None]



class MultipleTextInput(MultipleHiddenInput):
    """
    A widget that handles <input type="hidden"> for fields that have a list
    of values.
    """
    def __init__(self, attrs=None, choices=()):
        super(MultipleTextInput, self).__init__(attrs)
        self.input_type = 'text'
        # choices can be any iterable
        self.choices = choices

class AutocompleteInput(MultipleHiddenInput):
    pass

class UploadFile(MultiWidget):
    """
    Вджет для использование совместно с plupload.js http://www.plupload.com/, который дорисовывает <input type="file">
    """
    class Media:
        js = ('js/plupload/plupload.full.js', 'js/plupload/i18n/ru.js')
        #js = ('plupload/plupload.dev.js','plupload/plupload.flash.dev.js','plupload/plupload.html5.dev.js', 'plupload/i18n/ru.js')

    def __init__(self, attrs=None, input_attrs=None):
        input_attrs = input_attrs or {}
        input_attrs.update({
            'data-file':'name',
            'readonly':'readonly',
        })
        widgets = (
            HiddenInput(attrs={'data-file':'pk'}), # pk
            HiddenInput(attrs={'data-file':'url'}), # url
            AppendedText(attrs=input_attrs) # name для юзера
        )
        super(UploadFile, self).__init__(widgets, attrs)

    def decompress(self, value):
        if value:
            return value.split('$$')
        return [None, None, None]

class DateRangeWidget(MultiWidget):
    """
    A Widget that splits datetime input into two <input type="text"> boxes.
    """
    class Media:
        js = ('js/bootstrap-datepicker.js',)
        css = {
            'all':('css/datepicker.css',),
        }

    def __init__(self, attrs={}, date_format=None):
        attrs.update(readonly='readonly')
        widgets = (DateInput(attrs=attrs, format=date_format),
                   DateInput(attrs=attrs, format=date_format))
        super(DateRangeWidget, self).__init__(widgets, attrs)

    def decompress(self, value):
        if value:
            return map(lambda x: date.fromtimestamp(x), value.split('-'))
        return [None, None]

    def format_output(self, rendered_widgets):
        widgets = []
        for i, w in enumerate(rendered_widgets):
            widgets.append(mark_safe(u'<span class="date">%s<span class="add-on"><i class="icon-calendar"></i></span></span>' % w))

        return u'&nbsp;'.join(widgets)

class DateHiddenRangeWidget(DateRangeWidget):
    """
    A Widget that splits datetime input into two <input type="hidden"> inputs.
    """
    is_hidden = True

    def __init__(self, attrs=None, date_format=None):
        super(DateHiddenRangeWidget, self).__init__(attrs, date_format)
        for widget in self.widgets:
            widget.input_type = 'hidden'
            widget.is_hidden = True


class CheckboxRules(CheckboxInput):
    pass

class ColorPicker(AppendedText):
    class Media:
        js = ('js/bootstrap-colorpicker.js', 'js/forms/colorpicker.js')
        css = {
            'all':('css/colorpicker.css',)
        }
    def __init__(self, attrs={}):
        _cls = attrs.get('class', '') + ' input-mini' # input-color-picker'
        attrs.update({'class':' '.join(set(_cls.split()))})
        super(ColorPicker, self).__init__(attrs)
